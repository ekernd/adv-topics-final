YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "Institution Data Access",
        "Institution Data Access\nThis class allows the user to search for and instituion by name as choosen by impimentation of bootstrap3 typeahead plugin.",
        "Institution Details",
        "institutionDataAccess",
        "institutionList"
    ],
    "modules": [
        "Acme",
        "Amce"
    ],
    "allModules": [
        {
            "displayName": "Acme",
            "name": "Acme"
        },
        {
            "displayName": "Amce",
            "name": "Amce"
        }
    ],
    "elements": []
} };
});
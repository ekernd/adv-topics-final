<!DOCTYPE html>
<html>
<head>
	<title>Institution Service API Docs</title>
</head>
<body>

<h1>Official API Docs for the Institutions Web Service</h1>
  <table border="1">
    <tr>
      <th>API CALL (ROUTE)</th>
      <th>METHOD</th>
      <th>ACTION</th>
    </tr>
    <tr>
      <td>institutions/</td>
      <td>GET</td>
      <td>Get's all institutions</td>
    </tr>
    <tr>
      <td>institutions/</td>
      <td>POST</td>
      <td>
        Inserts a new institution into the data base <br>
        Expects the Content-Type to be application/json
      </td>
    </tr>
    <tr>
      <td>institutions/?order_by=x</td>
      <td>GET</td>
      <td>
        Get's all institutions, ordered by a property of a institution <br>
        Note: x could be 'name' or 'OPEID'
      </td>
    </tr>
    <tr>
      <td>institutions/x</td>
      <td>GET</td>
      <td>
        Get's a institution by it's OPEID property <br>
        ex: <b>institutions/000001</b> would fetch the institution with an id of 000001
      </td>
    </tr>
    <tr>
      <td>institutions/x</td>
      <td>PUT</td>
      <td>
        Edits the institution with id of x <br>
        Note: then Content-Type should be application/json
      </td>
    </tr>
  </table>
  ***By default, responses will return data in JSON format <br>
  ***BUT IF THE 'Accept' request header is set to 'application/xml' THEN WE RETURN THE DATA IN XML FORMAT

</body>
</html>
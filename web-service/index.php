<?php
require_once("includes/config.php");
header("Access-Control-Allow-Origin: *");
// Pre-requisites:
// 	1. Url Re-writing (mod rewrite)
//	2. AJAX - We should have completed the Abstracting AJAX project
//	3. Regular expressions


///////////////////////////////////
// Set up the data access object
///////////////////////////////////
$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
require("InstitutionDataAccess.inc.php");
$da = new institutionDataAccess($link);

// var_dump($da);

///////////////////////////////////
// Handle the Request
///////////////////////////////////

// Gather all the information about the request
$url_path = isset($_GET['url_path']) ? $_GET['url_path'] : ""; // This is the path part of the URL being requested (see the .htaccess file)
$method = $_SERVER['REQUEST_METHOD'];
$request_body = file_get_contents('php://input'); // note that GET requests do not have request body
$request_headers = getallheaders();


// This IF statement will check to see if the requested URL (and method) is supported by this web service
// If so, then we need to formulate the proper response, if not then we return a 404 status code
if($method == "GET" && empty($url_path)){

 	//Show the home page for this web service
  require("api-docs.php");
  die();

}else if($method == "GET" && ($url_path == "institutions/" || $url_path == "institutions")){

  get_all_institutions();

}else if($method == "POST" && ($url_path == "institutions/" || $url_path == "institutions")){
  	
  insert_institution();
  
}else if($method == "GET" && preg_match('/^institutions\/([0-9]*\/?)$/', $url_path, $matches)){
   
  // Get the id of the institution from the regular expression
  $institution_id = $matches[1];
  get_institution_by_id($institution_id);

}else if($method == "PUT" && preg_match('/^institutions\/([0-9]*\/?)$/', $url_path, $matches)){

  // Get the id of the institution from the regular expression
  //$institution_id = $matches[1]; // we can get the institution id from the request body
  update_institution();  
  
}else{

  header('HTTP/1.1 404 Not Found', true, 404);
  die("We're sorry, we can't find this page: {$_SERVER['REQUEST_URI']}");

}


/////////////////////////////////
// FUNCTIONS
/////////////////////////////////

function get_all_institutions(){

  global $da, $url_path, $method, $request_body, $request_headers;
  // var_dump($da);
  // var_dump($url_path);
  // check to see if we need to sort the institutions by author or title
  $order_by = isset($_GET['order_by']) ? $_GET['order_by'] : null;
  
  // get the institutions from the database
  $institutions = $da->get_all_institutions($order_by);
 
  if(!$institutions){
    header('HTTP/1.1 500 server error (fetching institutions from the database)', true, 500);
    die();
  }
    
  // We'll default to returning institutions in JSON format unless the client
  // requests XML (the Accept header in the request would be set to application/xml)
  $return_format = $request_headers['Accept'];
  
  if($return_format == "application/xml"){
    //header("Content-Type","application/xml");
    die("TODO: convert institution data to XML");
  }else{
    header("Content-Type","application/json");
    $json = json_encode($institutions);
    echo($json);
    die();
  }

}


function insert_institution(){

  global $da, $url_path, $method, $request_body, $request_headers;

  // The data for the institution being inserted should be in the request body.
  // The data should be sent in the JSON format which means that the Content-Type header in the request SHOULD be set to application/json
  // But if it's not set properly, we'll just assume the data is coming in as JSON
  // In the future, we might also accept the data in the request body to be XML, which would mean that the Content-Type header is set to application/xml
  
  $new_institution = null;
  
  if($request_headers['Content-Type'] == "application/xml"){
    die("TODO: convert XML institution data into an associative array");    
  }else{
    // convert the json to an associative array
    // note: json_decode() will return false if it can't convert the request body (maybe because it's not valid json)
    $new_institution = json_decode($request_body, true);
  }

  // TODO: validate the $new_institution assoc array (make sure it has id, title and author keys)

  if($new_institution){
    
    if($new_institution = $da->insert_institution($new_institution)){
      // note that the institution returned by insert_institution() will have the id set (by the database auto increment)
      // TODO: if the Accept header in the request is set to application/xml, then the client want the data returned in XML, we could deal with that later  
      // For now, we'll just return the data in JSON format
      header("Content-Type","application/json");
      die(json_encode($new_institution));
    }else{
      header('HTTP/1.1 500 server error (fetching institutions from the database)', true, 500);
      die();  
    }
  
  }else{
    header('HTTP/1.1 400 - the data sent in the request is not valid', true, 400);
    die();
  }

}


function get_institution_by_id($id){

  global $da, $url_path, $method, $request_body, $request_headers;

  $institution = $da->get_institution_by_id($id);

  // TODO: we may want to check the Accept header in the request, and if it's set to application/xml, we might return the data as XML instead of JSON
  // But for now, we'll just return the institution data as JSON
  
  if($institution){
    header("Content-Type","application/json");
    die(json_encode($institution));
  }else{
    header('HTTP/1.1 400 - the institution id in the requested url is not in the database', true, 400);
    die();
  }

}


function update_institution(){

  global $da, $url_path, $method, $request_body, $request_headers;

  if($institution = json_decode($request_body, true)){

    $institution = $da->update_institution($institution);

    if($institution){
      header("Content-Type","application/json");
      die(json_encode($institution));
    }else{
      header('HTTP/1.1 500 - Unable to update institution in the database', true, 500);
      die();
    }

  }else{
    header('HTTP/1.1 400 - Invalid institution data in the request body', true, 400);
    die();
  }

}
?>
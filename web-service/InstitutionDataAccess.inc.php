<?php

class institutionDataAccess{
	
	private $link;

	/**
	* Constructor
	* @param $link 		The connection to the database
	*/
	function __construct($link){
		$this->link = $link;
	}


	/**
	* Get all institutions
	* @param $order_by 		Optional - the sort order (name or OPEID)
	* @return 2d array 		Returns an array of institutions (each institution is an assoc array)
	*/
	function get_all_institutions($order_by = null){
		// TODO: if the order_by param is not null we need to sort the institutions (should be by title or author)
		$qStr = "SELECT	id, name, longitude, latitude, OPEID FROM institutions";

		if($order_by == "name" || $order_by == "OPEID"){
			$qStr .= " ORDER BY " . $order_by;
		}
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		$all_institutions = array();

		while($row = mysqli_fetch_assoc($result)){

			$institution = array();
			$institution['id'] = htmlentities($row['id']);
			$institution['name'] = htmlentities($row['name']);
			$institution['longitude'] = htmlentities($row['longitude']);
			$institution['latitude'] = htmlentities($row['latitude']);
			$institution['OPEID'] = htmlentities($row['OPEID']);

			$all_institutions[] = $institution;
		}

		return $all_institutions;
	}


	/**
	* Get a institution by its ID
	* @param $opeid			The OPEID of the institution to get
	* @return array 		An assoc array that has keys for each property of the institution
	*/
	function get_institution_by_id($opeid){
		
		// TODO: if the order_by param is not null we need to sort the institutions (should be by title or author)
		$qStr = "SELECT	id, name, longitude, latitude, OPEID FROM institutions WHERE OPEID = " . mysqli_real_escape_string($this->link, $opeid);

				
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result->num_rows == 1){
			$row = mysqli_fetch_assoc($result);

			$institution = array();
			$institution['id'] = htmlentities($row['id']);
			$institution['name'] = htmlentities($row['name']);
			$institution['longitude'] = htmlentities($row['longitude']);
			$institution['latitude'] = htmlentities($row['latitude']);
			$institution['OPEID'] = htmlentities($row['OPEID']);
			return $institution;

		}else{
			return null;
		}
			
	}


	function insert_institution($institution){

		$institution['name'] = mysqli_real_escape_string($this->link, $institution['name']);
		$institution['longitude'] = mysqli_real_escape_string($this->link, $institution['longitude']);
		$institution['latitude'] = mysqli_real_escape_string($this->link, $institution['latitude']);
		$institution['OPEID'] = mysqli_real_escape_string($this->link, $institution['OPEID']);
		

		$qStr = "INSERT INTO institutions (
					name,
					longitude,
					latitude,
					OPEID
				) VALUES (
					'{$institution['name']}',
					'{$institution['longitude']}'
					'{$institution['latitude']}'
					'{$institution['OPEID']}'
				)";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the institution id that was assigned by the data base
			$institution['id'] = mysqli_insert_id($this->link);
			// then return the institution
			return $institution;
		}else{
			$this->handle_error("unable to insert institution");
		}

		return false;
	}



	function update_institution($institution){

		// prevent SQL injection
		// $institution['id'] = mysqli_real_escape_string($this->link, $institution['id']);
		// $institution['title'] = mysqli_real_escape_string($this->link, $institution['title']);
		// $institution['author'] = mysqli_real_escape_string($this->link, $institution['author']);

		
		$institution['name'] = mysqli_real_escape_string($this->link, $institution['name']);
		$institution['longitude'] = mysqli_real_escape_string($this->link, $institution['longitude']);
		$institution['latitude'] = mysqli_real_escape_string($this->link, $institution['latitude']);
		$institution['OPEID'] = mysqli_real_escape_string($this->link, $institution['OPEID']);

		$qStr = "UPDATE institutions SET name='{$institution['name']}',
		 longitude='{$institution['longitude']}',
		 latitude = '{$institution['latitude']}',
		 OPEID= '{$institution['OPEID']}'
		 WHERE id ={$institution['id']}";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $institution;
		}else{
			$this->handle_error("unable to update institution");
		}

		return false;
	}
	


}
/**
@module Acme
@class Institution Details
@method institutionDetails
 */

var acme = acme || {};

/**

 */

acme.institutionDetails = {
  /**
   * Description
   * @method init
   * @param options.mapDiv {string} name of the div where inst map will be placed.
   * @return ObjectExpression
   */
  init: function(options) {
    // Instance Vars

    var div = options.div;
    var institution = options.institution;

    var mapDiv = options.mapDiv || null;

    var txtName;
    var txtID;
    var txtLongitude;
    var txtLatitude;
    var txtOPEID;
    var button;
    var institutionName;
    /**
     * The destroyUI deletes and resets the div that renders the institution OPEID
     * @method destroyUI
     * @return
     */
    function destroyUI() {
      var targetDiv = div;
      while (targetDiv.firstChild) {
        targetDiv.removeChild(targetDiv.firstChild);
      }

      var targetDiv2 = mapDiv;
      while (targetDiv2.firstChild) {
        targetDiv2.removeChild(targetDiv2.firstChild);
      }
    }
    /**
     * The createUI method is used to render the user input
     * @method createUI
     * @return
     */
    function createUI() {
      //debugger;
      // console.log(div);
      institutionObj = JSON.parse(institution);
      divID = div;
      // console.log(institution);

      var targetDiv = div;
      var createForm = document.createElement("form");

      // var numOfinstitutionsinDb = institutions.length;
      var newLine = document.createElement("br");
      var heading = document.createElement("h4"); // Heading of Form
      // var hString = "institution to Edit";
      // heading.innerHTML = hString;
      // <form onsubmit="checkform()" , name="institutionManipulationForm">
      createForm.setAttribute("id", "institutionsForm");
      createForm.setAttribute("name", "institutionManipulationForm");
      // createForm.setAttribute("class", "form-control");
      //createForm.setAttribute("onsubmit", saveCallback);
      createForm.appendChild(heading);
      var lineBreak = document.createElement("br");
      createForm.appendChild(lineBreak);

      var nameLabel = document.createElement("label"); // Create Label for Name Field
      nameLabel.innerHTML = "institution name:"; // Set Field Labels
      // createForm.appendChild(nameLabel);

      // var lineBreak = document.createElement("br");
      // createForm.appendChild(lineBreak);

      var inputElement = document.createElement("input"); // Create Input Field for Name
      inputElement.setAttribute("type", "text");
      inputElement.setAttribute("id", "institutionID");
      inputElement.setAttribute("value", institutionObj.id);
      inputElement.setAttribute("name", "txtID");
      inputElement.style.display = "none";
      inputElement.setAttribute("visibility", "hidden");
      inputElement.setAttribute("onkeyup", "expand(this)");
      createForm.appendChild(inputElement);

      //  console.log("the institution name is " + institutionObj.name);
      var inputElement = document.createElement("input"); // Create Input Field for Name
      inputElement.setAttribute("type", "text");
      inputElement.setAttribute("institutionName", "institutionName");
      inputElement.setAttribute("value", institutionObj.name);
      inputElement.setAttribute("name", "txtName");
      inputElement.setAttribute("id", "institutionName");
      // createForm.appendChild(inputElement);

      var lineBreak = document.createElement("br");
      createForm.appendChild(lineBreak);

      var opeidLabel = document.createElement("label"); // Create Label for Name Field
      opeidLabel.innerHTML = "Institution OPEID: "; // Set Field Labels
      createForm.appendChild(opeidLabel);
      var lineBreak = document.createElement("br");
      createForm.appendChild(lineBreak);
      var inputElement = document.createElement("input"); // Create Input Field for Name
      inputElement.setAttribute("type", "text");
      inputElement.setAttribute("instituionOPEID", "instituionOPEID");
      inputElement.setAttribute("name", "txtOPEID");
      inputElement.setAttribute("value", institutionObj.OPEID);
      inputElement.setAttribute("class", "typeahead form-control");
      inputElement.setAttribute("onkeyup", "expand(this)");
      inputElement.setAttribute("id", "instituionOPEID");
      createForm.appendChild(inputElement);

      var lineBreak = document.createElement("br");
      createForm.appendChild(lineBreak);
      ///var expandTest = document.crea;

      var btnSave = document.createElement("button");
      btnSave.setAttribute("data-institutionId-forPut", institutionObj.id);
      btnSave.setAttribute("type", "button");
      // btnSave.setAttribute("name", "txtID");
      btnSave.innerHTML = "SAVE";
      btnSave.addEventListener("click", function() {
        // console.log("click");
        // saveCallback();
      });

      // createForm.appendChild(btnSave);

      targetDiv.appendChild(createForm);

      var scriptToAdd = document.createElement("script");
      var s =
        "function expand(textbox) {var minSize = 20;var contentSize =\
              textbox.value.length;if (contentSize > minSize)\
              {textboxSize = contentSize;} else {textboxSize = minSize;}};";
      scriptToAdd.innerHTML = s;
      targetDiv.appendChild(scriptToAdd);
    }
    /**
     * The initMap function in the callback that is utilized by the Google Maps API
     * @method initMap
     * @return
     */
    function initMap() {
      var long = institutionObj.longitude;
      var lat = institutionObj.latitude;
      console.log(institutionObj);

      var pinLocation = new google.maps.LatLng(lat, long);
      var myLatLng = { lat: lat, lng: long };
      // var myLatLng = { lat: -25.363, lng: 131.044 };
      // var pinLocation = new google.maps.LatLng(40.78271, -73.96531);
      // console.log("map" + institutionName);
      var startPosition = new google.maps.Marker({
        position: pinLocation,
        title: institutionObj.name,
        map: venueMap
      });
      // console.dir(startPosition);
      var mapOptions = {
        zoom: 12,
        center: pinLocation,
        // icon: google.maps.SymbolPath.CIRCLE,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: true,
        zoomControl: true,
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.SMALL,
          position: google.maps.ControlPosition.TOP_RIGHT
        },

        mapTypeControl: true,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          position: google.maps.ControlPosition.TOP_LEFT
        },

        scaleControl: true,
        scaleControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER
        },
        streetViewControl: false,
        overviewMapControl: false

        // styles: [
        //   {
        //     stylers: [{ hue: "#00ff6f" }, { saturation: -50 }]
        //   },
        //   {
        //     featureType: "road",
        //     elementType: "geometry",
        //     stylers: [{ lightness: 100 }, { visibility: "simplified" }]
        //   },
        //   {
        //     featureType: "transit",
        //     elementType: "geometry",
        //     stylers: [{ hue: "#ff6600" }, { saturation: +80 }]
        //   },
        //   {
        //     featureType: "transit",
        //     elementType: "labels",
        //     stylers: [{ hue: "#ff0066" }, { saturation: +80 }]
        //   },
        //   {
        //     featureType: "poi",
        //     elementType: "labels",
        //     stylers: [{ visibility: "off" }]
        //   },
        //   {
        //     featureType: "poi.park",
        //     elementType: "labels",
        //     stylers: [{ visibility: "on" }]
        //   },
        //   {
        //     featureType: "water",
        //     elementType: "geometry",
        //     stylers: [{ hue: "#c4f4f4" }]
        //   },
        //   {
        //     featureType: "road",
        //     elementType: "labels",
        //     stylers: [{ visibility: "off" }]
        //   }
        // ]
      };

      var venueMap = new google.maps.Map(mapDiv, mapOptions);
      startPosition.setMap(venueMap);
    }

    /**
     * Description
     * @method setInstitution
     * @param {} newInstitution
     * @return
     */
    function setInstitution(newInstitution) {
      institution = newInstitution;
      txtID.value = institution.id;
      txtName.value = institution.name;
      txtOPEID.value = institution.OPEID;
    }

    // Return the public API
    return {
      setInstitution: setInstitution,
      createUI: createUI,
      destroyUI: destroyUI,
      initMap: initMap
    };
    // mapInstitution:mapInstitution
  }
};

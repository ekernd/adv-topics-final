/**
@module Acme
@class institutionList
@method institutionList
 */

/**

 */
var acme = acme || {};

acme.institutionList = {
  /**
   * Description
   * @method init
   * @param options.institutions {string} Array of json string that contain all institution data.
   * @return ObjectExpression
   */
  init: function(options) {
    // Instance Vars
    var target = options.div;
    var institutions = options.institutions;
    var saveCallback = options.saveCallback || null;

    var ul;

    target.innerHTML = ""; // there are better ways to clear out the target element
    var welcome = document.createElement("h1");
    var welcomeStr = "Welcome!";
    var heading = document.createElement("h5"); // Heading of Form
    var newLine = document.createElement("br");
    var inst = document.createElement("p");
    var i = document.createElement("p");
    i.style.fontSize = "x-small";
    var hString =
      convertNumberToWords(institutions.length) +
      " institutions Currently in DataBase. ";
    var source =
      '<a href="https://nces.ed.gov/ipeds/Home/UseTheData" target="_blank" >raw source:  IPEDS Data Center - 15/16 Final Release </a>';
    var instructions = 'Please begin typing your institution name below.  Once you see the one you want, select it by using the up and down arrow keys. Then click the "Search" box or press the Enter key. ';
    inst.innerHTML = instructions;
    welcome.innerHTML = welcomeStr;
    heading.innerHTML = hString;
    i.innerHTML = source;
    target.appendChild(welcome);
    target.appendChild(heading);

    target.appendChild(inst);
    target.appendChild(i);
    target.appendChild(newLine);
    // target.appendChild(generateList());

    /**
     * Description
     * @method generateList
     * @return ul
     */
    function generateList() {
      ul = document.createElement("ul");
      var li;
      var btn;
      var institution;
      //debugger;
      var liNew = document.createElement("li");
      var btnNew = document.createElement("input");
      btnNew.type = "button";
      btnNew.value = "Enter a New institution";
      btnNew.setAttribute("data-institutionId", -1);
      var newinstitution = {
        id: "-1",
        name: "",
        longitude: "",
        latitude: "",
        OPEID: ""
      };
      var newLine = document.createElement("br");
      btnNew.appendChild(newLine);
      btnNew.institution = newinstitution;
      liNew.appendChild(btnNew);

      ul.appendChild(liNew);
      var instCount = 0;
      for (x in institutions) {
        if (instCount > 20) {
          break;
        }
        institution = institutions[x];

        li = document.createElement("li");
        li.innerHTML =
          "<br><br>" +
          "Name: " +
          institution.name +
          "<br>" +
          " Longitude: " +
          institution.longitude +
          "<br>" +
          " Latitude: " +
          institution.latitude +
          "<br>" +
          " OPEID: " +
          institution.OPEID +
          "<br>";
        // newLine = document.createElement("br");

        btn = document.createElement("input");
        btn.type = "button";
        btn.value = "Edit";
        btn.institution = institution;
        btn.setAttribute("data-institutionId", institution.OPEID);

        li.appendChild(btn);
        ul.appendChild(li);

        instCount++;
      }
      return ul;
    }

    //Source for convertNumberToWords:   https://jsfiddle.net/lesson8/5tt7d3e6/
    /**
     * Description
     * @method convertNumberToWords
     * @param {} amount
     * @return words_string
     */
    function convertNumberToWords(amount) {
      var words = new Array();
      words[0] = "";
      words[1] = "One";
      words[2] = "Two";
      words[3] = "Three";
      words[4] = "Four";
      words[5] = "Five";
      words[6] = "Six";
      words[7] = "Seven";
      words[8] = "Eight";
      words[9] = "Nine";
      words[10] = "Ten";
      words[11] = "Eleven";
      words[12] = "Twelve";
      words[13] = "Thirteen";
      words[14] = "Fourteen";
      words[15] = "Fifteen";
      words[16] = "Sixteen";
      words[17] = "Seventeen";
      words[18] = "Eighteen";
      words[19] = "Nineteen";
      words[20] = "Twenty";
      words[30] = "Thirty";
      words[40] = "Forty";
      words[50] = "Fifty";
      words[60] = "Sixty";
      words[70] = "Seventy";
      words[80] = "Eighty";
      words[90] = "Ninety";
      amount = amount.toString();
      var atemp = amount.split(".");
      var number = atemp[0].split(",").join("");
      var n_length = number.length;
      var words_string = "";
      if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
          received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
          n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
          if (i == 0 || i == 2 || i == 4 || i == 7) {
            if (n_array[i] == 1) {
              n_array[j] = 10 + parseInt(n_array[j]);
              n_array[i] = 0;
            }
          }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
          if (i == 0 || i == 2 || i == 4 || i == 7) {
            value = n_array[i] * 10;
          } else {
            value = n_array[i];
          }
          if (value != 0) {
            words_string += words[value] + " ";
          }
          if (
            (i == 1 && value != 0) ||
            (i == 0 && value != 0 && n_array[i + 1] == 0)
          ) {
            words_string += "Crores ";
          }
          if (
            (i == 3 && value != 0) ||
            (i == 2 && value != 0 && n_array[i + 1] == 0)
          ) {
            words_string += "Lakhs ";
          }
          if (
            (i == 5 && value != 0) ||
            (i == 4 && value != 0 && n_array[i + 1] == 0)
          ) {
            words_string += "Thousand ";
          }
          if (
            i == 6 &&
            value != 0 &&
            (n_array[i + 1] != 0 && n_array[i + 2] != 0)
          ) {
            words_string += "Hundred and ";
          } else if (i == 6 && value != 0) {
            words_string += "Hundred ";
          }
        }
        words_string = words_string.split("  ").join(" ");
      }
      return words_string;
    }

    return { generateList: generateList };
  }
};

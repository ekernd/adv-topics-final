/**
* @module Amce


@class Institution Data Access
*/

var acme = acme || {};

var urlWebservice = "";
console.log(location.host);
if (location.host == "localhost:8080") {
  urlWebservice = "http://localhost:8080";
} else {
  urlWebservice = "http://www.daveekern.com";
}

/**
 * @class Institution Data Access
 * This class allows the user to search for and instituion by name as choosen by impimentation of bootstrap3 typeahead plugin.
 */

/**
 * Description
 * @class institutionDataAccess
 * @method institutionDataAccess
 * @return ObjectExpression
 */
acme.institutionDataAccess = function() {
  var ajax = acme.ajax;

  if (!ajax) {
    throw new Error("This module must have access to acme.ajax");
  }
  /**
   * The getAllinstitutions performs an ajax request to the webservice and data returned is used in the callback method
   * @method getAllinstitutions
   * @param {} callback
   * @return
   */
  function getAllinstitutions(callback) {
    acme.ajax.send({
      callback: callback,
      url: urlWebservice + "/adv-topics-final/web-service/institutions/",
      method: "GET",
      headers: {
        "Content-Type": "application/xml",
        "Cache-Control": "no-cache"
      }
    });
  }
  /**
   * The getAllinstitutions performs an ajax request to the webservice and data returned is used in the callback method
   * @method getInstitutionByOPEID
   * @param OPEID {String} OPEID if the institution
   * @param {} callback
   * @return
   */
  function getInstitutionByOPEID(OPEID, callback) {
    // TODO: use your acme.ajax module to send the proper request to the institution web service
    // to get a institution by it's OPEID

    acme.ajax.send({
      callback: callback,
      url:
        urlWebservice + "/adv-topics-final/web-service/institutions/" + OPEID,
      method: "GET",
      headers: {
        "Content-Type": "application/xml",
        "Cache-Control": "no-cache"
      }
    });
  }

  /**
   * The getAllinstitutions performs an ajax request to the webservice and data returned is used in the callback method
   * @method postInstitution
   * @param institution {Object} Object that describes an institution to post to the database
   * @param {} callback
   * @return
   */
  function postInstitution(institution, callback) {
    // TODO: use your acme.ajax module to send the proper request to the institution web service
    // in order to add a institution

    acme.ajax.send({
      callback: callback,
      url: urlWebservice + "/adv-topics-final/web-service/institutions/",
      method: "POST",
      //	headers: { "Content-Type": "application/xml", "Cache-Control": "no-cache" },
      data: JSON.stringify(institution)
    });
  }
  /**
   * The putInstitution performs an ajax request to the webservice and data returned is used in the callback method
   * @method putInstitution
   * @param institution {Object} Object that describes an institution to put in the database
   * @param {} callback
   * @return
   */
  function putInstitution(institution, callback) {
    // TODO: use your acme.ajax module to send the proper request to the institution web service
    // in order to add a institution
    acme.ajax.send({
      callback: callback,
      url:
        urlWebservice +
        "/adv-topics-final/web-service/institutions/" +
        institution.id,
      method: "PUT",
      headers: {
        "Content-Type": "application/xml",
        "Cache-Control": "no-cache"
      },
      data: JSON.stringify(institution)
    });
  }
  //return public api
  return {
    getAllinstitutions: getAllinstitutions,
    getInstitutionByOPEID: getInstitutionByOPEID,
    postInstitution: postInstitution,
    putInstitution: putInstitution
  };
};

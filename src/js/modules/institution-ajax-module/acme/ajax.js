/**
 
@module Acme
@main This is my acme package  //description of the module
 */
var acme = acme || {}; //if acme exists set acme to acme, if acme does not exist else set acme to {} <==emtpy object

/**
@method ajax
*/
acme.ajax = {
  /**
   * Description
   * @method send
   * @param {} options
   * @return
   */
  send: function(options) {
    var http = new XMLHttpRequest();
    var callback = options.callback;
    var headers = options.headers;
    var url = options.url;
    var method = options.method;
    var data = options.data;
    //var http = new XMLHttpRequest();
    http.open(method, url);

    for (var key in headers) {
      http.setRequestHeader(key, headers[key]);
    }

    http.addEventListener("readystatechange", function() {
      if (http.readyState == 4 && (http.status == 200 || http.status == 0)) {
        callback(http.responseText);
      } else if (http.readyState == 4) {
        alert("http status " + http.status + "ajax error " + ln());
      }
    });

    //var requestBody = url;
    http.send(data);
  },
  /**
   * Description
   * @method error
   * @param {} errMsg
   * @return
   */
  error: function(errMsg) {
    alert("ajax error " + ln() + errMsg);
  }
};

//get line number source https://stackoverflow.com/questions/2343343/how-can-i-determine-the-current-line-number-in-javascript
/**
 * Description
 * @method ln
 * @return MemberExpression
 */
function ln() {
  var e = new Error();
  if (!e.stack)
    try {
      // IE requires the Error to actually be throw or else the Error's 'stack'
      // property is undefined.
      throw e;
    } catch (e) {
      if (!e.stack) {
        return 0; // IE < 10, likely
      }
    }
  var stack = e.stack.toString().split(/\r\n|\n/);
  // We want our caller's frame. It's index into |stack| depends on the
  // browser and browser version, so we need to search for the second frame:
  var frameRE = /:(\d+):(?:\d+)[^\d]*$/;
  do {
    var frame = stack.shift();
  } while (!frameRE.exec(frame) && stack.length);
  return frameRE.exec(stack.shift())[1];
}

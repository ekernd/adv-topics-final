window.addEventListener("load", function () {
    var responseData;
    var searchTextReturned;
    var da = acme.institutionDataAccess();
    var numOfinstitutionsinDb = 0;
    var clearButtonDiv = document.getElementById("clearButton");
    da.getAllinstitutions(function (response) {
        var institutions = JSON.parse(response);
        responseData = institutions;
        numOfinstitutionsinDb = institutions.length;
        console.log(numOfinstitutionsinDb);
        var div = document.getElementById("institution-list");
        var institutionList = acme.institutionList.init({
            div: div, //
            institutions: institutions
        });
        institutionList.generateList();
    });

    var urlWebservice = "";
    console.log(location.host);
    if (location.host == "localhost:8080") {
        urlWebservice = "http://localhost:8080";
    } else {
        urlWebservice = "http://www.daveekern.com";
    }
    var theData;
    $.get(
        urlWebservice + "/adv-topics-final/web-service/institutions/",
        function (data) {
            $("#name").typeahead({ source: data });
        },
        "json"
    );

    var instObj = document.getElementById("name");
    //    console.log(instObj);

    //console.log(selectedInstName);
    var instSelectedObject;

    // https://stackoverflow.com/questions/8796988/binding-multiple-events-to-a-listener-without-jquery

    var searchCount = 0;


    var searchButtonDiv = document.getElementById("searchButton");
    var searchButton = document.createElement('button');
    var searchText = document.createTextNode('Search');
    searchButton.appendChild(searchText);
    searchButtonDiv.appendChild(searchButton);
    searchButtonDiv.addEventListener('click', searchForDetails);





    instObj.addEventListener("keydown", searchForDetails);

    instObj.addEventListener("focus", resetSearch);

    /**
     * Description
     * @method resetSearch
     * @return
     */
    function resetSearch() {
        instObj.value = "";
        clearButton.innerHTML = "";
        searchCount = 0;
        var mapDiv = document.getElementById("map");
        var targetDiv = document.getElementById("institution-details");
        var institutionString = "";

        var institutionDetails = acme.institutionDetails.init({
            institution: institutionString,
            div: targetDiv,
            mapDiv: mapDiv
        });

        institutionDetails.destroyUI();
    }

    /**
     * Description
     * @method searchForDetails
     * @param {} e
     * @return
     */
    function searchForDetails(e) {
        console.dir(e.type);
        // if (e.type == "blur") {
        //     e.keyCode = 13;
        // }
        if ((13 == e.keyCode && searchCount === 0) || ('click' == e.type && searchCount === 0)) {





            searchCount++;
            var selectedInstName = document.querySelector("#name").value;
            console.log(selectedInstName);
            instSelectedObject = search(selectedInstName, responseData);
            console.log("search obj returned" + instSelectedObject);

            if (instSelectedObject == undefined) {
                var noResponseDiv = document.getElementById('map');
                noResponseDiv.setAttribute("class", "popup");
                var noRespPopup = document.createElement('span');
                noRespPopup.setAttribute("class", "popuptext");

                noRespPopup.innerText = "A search of '" + selectedInstName + " ' did not return any results.";

                noResponseDiv.appendChild(noRespPopup);
            }
            console.log("search obj :  " + JSON.stringify(instSelectedObject));
            // console.log(instSelectedObject);
            // debugger;

            if (instSelectedObject == undefined) {
                instObj.focus;
            } else {
                var targetDiv = document.getElementById("institution-details");
                var mapDiv = document.getElementById("map");
                //  console.log(targetDiv);
                //  console.log(institutionForDetailsForm);
                var institutionString = JSON.stringify(instSelectedObject);
                var institutionDetails = acme.institutionDetails.init({
                    institution: institutionString,
                    div: targetDiv,
                    mapDiv: mapDiv
                });
                institutionDetails.createUI();
                institutionDetails.initMap();
                // topFunction();
            }


            var clearButton = document.createElement("button");
            var clearText = document.createTextNode("Clear Search");
            clearButton.appendChild(clearText);
            clearButtonDiv.appendChild(clearButton);
            clearButtonDiv.addEventListener("click", resetSearch);
            // console.log(instSelectedObject);
        }
    }

    /**
     * Description
     * @method search
     * @param {} nameKey
     * @param {} myArray
     * @return
     */
    function search(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (
                myArray[i].name.trim().toLowerCase() === nameKey.trim().toLowerCase()
            ) {
                console.log("from lookup: " + myArray[i]);
                searchTextReturned = myArray[i];
                return myArray[i];
            }
        }
    }
});

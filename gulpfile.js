/*
One thing I don't like about gulp.dest() is that you can't easily change the file name that you are moving!!!
*/

var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var cssimport = require("gulp-cssimport");
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var htmlreplace = require("gulp-html-replace");

/*
THIS TASK IS FOR USE IN DEVELOPMENT, YOU MAY WANT TO SET UP A WATCH FOR IT!
Run this task when you make changes to the scss file during development,
it will convert main.scss to main.css
*/
gulp.task("sass", function(){
	gulp.src("src/styles/main.scss")
        .pipe(sass({style: 'expanded'}))
        .pipe(gulp.dest("src/styles/"))
        .on('error', gutil.log)
});

gulp.task('watchsass', function() {
  gulp.watch('src/styles/main.scss', ['sass']);
});


/*this task does the following...
1. looks at the @import statements in main.scss and replaces the import statement with the actual code
2. processes all sass code into css code
3. minifies the css code
4. places the finished file in dist/styles/main.css
*/
gulp.task("processCSS", function() {
    gulp.src("src/styles/main.scss")
        .pipe(cssimport({}))
        .on('error', gutil.log)
        .pipe(sass({style: 'expanded'}))
    	.on('error', gutil.log)
    	.pipe(cleanCSS({compatibility: 'ie8'}))
    	.on('error', gutil.log)
  		.pipe(gulp.dest('dist/styles/'));
}); 


/*
This task will concatenate and minify all js files (be sure to include each of the js files in the param to src())
*/
gulp.task('processJS', function() {
  gulp
    .src([// https://en.wikipedia.org/wiki/Glob_%28programming%29
      // https://semaphoreci.com/community/tutorials/getting-started-with-gulp-js
      // "src/js/main.js",
      "src/js/modules/acme/ajax.js",
       "src/js/modules/acme/institution-details.js", 
       "src/js/modules/acme/institution-list-original.js", 
       "src/js/modules/acme/institutionDataAccess-ajax.js",
        "src/js/modules/institution-ajax-module/jquery.js",
      "src/js/modules/institution-ajax-module/bootstrap3-typeahead.min.js"
       ])
    .pipe(uglify())
    .pipe(concat("main.min.js"))
    .pipe(gulp.dest("dist/js"))
    .on("error", gutil.log);
});

/*
This task takes all the separate, invidual links to css and js files in the index html file
and replaces them with a link to the minified and concatenated file
*/
gulp.task('processHTML', function() {
  	gulp.src('src/index.html')
    .pipe(htmlreplace({'css': 'styles/main.css', 'js': 'js/main.min.js'}))
    .pipe(gulp.dest('dist/'))
    .on('error', gutil.log);
});


gulp.task('default', ['processCSS','processJS','processHTML']);